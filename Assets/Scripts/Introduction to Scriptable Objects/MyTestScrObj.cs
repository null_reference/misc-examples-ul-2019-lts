﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create new food")]
public class MyTestScrObj : ScriptableObject
{
    public string foodName;
    public int totalCalories;
    public bool needsCalories;
}
